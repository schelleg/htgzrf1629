all: bit bsp link
htgzrf1629.bit:
	make -C base
	cp base/vivado_project/base.runs/impl_1/design_1_wrapper.bit htgzrf1629.bit
htgzrf1629.bsp: htgzrf1629.bit
	make -C bsp
	cp bsp/htgzrf1629.bsp htgzrf1629.bsp
link:
	cd HTGZRF1629
	ln -sf ../htgzrf1629.bsp .
clean:
	rm -f base/vivado*.jou base/vivado*.log vivado*.jou vivado*.log
	rm -rf base/vivado_project
	rm -rf bsp/htgzrf1629 bsp/htgzrf1629.bsp
	rm -rf HTGZRF1629/htgzrf1629.bsp
