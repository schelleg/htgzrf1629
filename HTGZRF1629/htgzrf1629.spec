# Copyright (C) 2022 Xilinx, Inc
# SPDX-License-Identifier: BSD-3-Clause

ARCH_HTGZRF1629 := aarch64
BSP_HTGZRF1629 := htgzrf1629.bsp

STAGE4_PACKAGES_HTGZRF1629 := xrfclk xrfdc xsdfec ethernet
STAGE4_PACKAGES_HTGZRF1629 += xrt_zocl xrt
