petalinux-create -t project -n htgzrf1629 --template zynqMP --force #--tmpdir /tmp/peta
#chown u+w -R htgzrf1629
cp system-user.dtsi htgzrf1629/project-spec/meta-user/recipes-bsp/device-tree/files/
petalinux-config --get-hw-description ../base/vivado_project/design_1_wrapper.xsa -p htgzrf1629/ --silentconfig
cat config >> htgzrf1629/project-spec/configs/config
cat user-rootfsconfig >> htgzrf1629/project-spec/meta-user/conf/user-rootfsconfig
cat rootfs_config >> htgzrf1629/project-spec/configs/rootfs_config
#cat petalinuxbsp.conf>> htgzrf1629/project-spec/meta-user/conf/petalinuxbsp.conf
petalinux-config -p htgzrf1629/ --silentconfig
petalinux-config -c rootfs -p htgzrf1629/ --silentconfig
#petalinux-config -c rootfs -p htgzrf1629 --silentconfig
#petalinux-config -c kernel -p htgzrf1629 --silentconfig
#petalinux-config -c device-tree -p htgzrf1629 --silentconfig
#petalinux-config -c bootloader -p htgzrf1629 --silentconfig
#petalinux-config -c pmufw -p htgzrf1629 --silentconfig
#petalinux-config -c u-boot -p htgzrf1629 --silentconfig
#petalinux-config --boot-device sd -p htgzrf1629 --silentconfig

petalinux-build -p htgzrf1629
petalinux-package -p htgzrf1629 --boot --fsbl htgzrf1629/images/linux/zynqmp_fsbl.elf --fpga ../htgzrf1629.bit --pmufw htgzrf1629/images/linux/pmufw.elf --atf htgzrf1629/images/linux/bl31.elf --u-boot htgzrf1629/images/linux/u-boot.elf --force 
#petalinux-package --boot \
#	--fsbl images/linux/zynqmp_fsbl.elf \
#	--fpga ../htgzrf1629.bit \
#	--pmufw images/linux/pmufw.elf \
#	--atf images/linux/bl31.elf \
#	--u-boot images/linux/u-boot.elf \
#	-p htgzrf1629 \
#	--force
#petalinux-package --wic
petalinux-package --prebuilt -p htgzrf1629 --force
#petalinux-boot --qemu --prebuilt 3

petalinux-package --bsp -p htgzrf1629 -o htgzrf1629.bsp --force
