create_project base ./vivado_project/ -part xczu29dr-ffvf1760-2-e -force
create_bd_design "design_1"
source ps.tcl
source bd.tcl
generate_target all [get_files  design_1.bd]


make_wrapper -files [get_files design_1.bd] -top
add_files -norecurse vivado_project/base.gen/sources_1/bd/design_1/hdl/design_1_wrapper.v

add_files -fileset constrs_1 base.xdc

launch_runs impl_1 -to_step write_bitstream -jobs 12
wait_on_run impl_1
write_hw_platform -fixed -include_bit -force -file vivado_project/design_1_wrapper.xsa

